# -*- coding: utf-8 -*-
"""
Código para ejecutar varios modelos de Iber en serie

Gonzalo García-Alén Julio/2023
"""

import os
import sys
import subprocess

# Aquí indicamos la ruta de los modelos que queremos que sean ejecutados
ModelosIber = [r'...\Azud\Q100.gid',
               r'...\Azud\Q500.gid']

# Especificamos el ejecutable a usar, que ya debe estar en cada carpeta .gid
# Todos los ejecutables de Iber están en esta ruta dentro de la carpeta donde
# se ha instalado el programa: ...\Iber 3.3\problemtypes\IBER.gid\bin\windows
iber_exe = 'IberPlus.exe'  # 'Iber.exe', 'IberPlus.exe' o 'RIber.exe'


print('\tEjecutando modelos Iber:')
for i in range(len(ModelosIber)):
    os.chdir(ModelosIber[i])
    a = ('\t' + str(i+1) + '/' + str(len(ModelosIber)))
    sys.stdout.write('\r'+a)
    subprocess.call( iber_exe )
    if i == (len(ModelosIber)-1): print('\n')
